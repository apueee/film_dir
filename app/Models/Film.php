<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'films';

    /**
     * The attributes that uses for timestamps.
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'description','release_date','rating','ticket_price','country','photo'];


    public function comments()
    {
        return $this->hasMany('App\Models\Comment');

    }

    public function genres()
    {
        return $this->belongsToMany('App\Models\Genre', 'films_genres','film_id', 'genre_id');
    }
}
