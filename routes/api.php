<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/films/{slug}', ['as' => 'films.show', 'uses' => 'API\FilmController@show']);
Route::post('/films/{film}/comments', ['as' => 'films.show', 'uses' => 'API\FilmController@StoreComment']);

Route::apiResources([
    'films' => 'API\FilmController'
]);


