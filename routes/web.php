<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('films');
});





Auth::routes();


Route::group(['middleware' => 'auth'], function(){
    Route::get('/films/create', ['as' => 'films.create', 'uses' => 'FilmsController@create']);
    Route::post('/films', ['as' => 'films.store', 'uses' => 'FilmsController@store']);
    Route::post('/films/{film}/comments', ['as' => 'films.comments', 'uses' => 'FilmsController@StoreComment']);
});

Route::get('/films', ['as' => 'films.index', 'uses' => 'FilmsController@index']);
Route::get('/films/{slug}', ['as' => 'films.show', 'uses' => 'FilmsController@show']);
