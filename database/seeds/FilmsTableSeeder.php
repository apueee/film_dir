<?php

use Illuminate\Database\Seeder;
use \App\Models\Film;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Film::truncate();

        $faker = \Faker\Factory::create();

        for($i=0; $i < 3; $i++) {

            $name = $faker->sentence;

            $film = Film::create([
                'name' => $name ,
                'slug' => str_slug($name),
                'description' => $faker->paragraph,
                'release_date' => \Carbon\Carbon::tomorrow(),
                'rating' => $faker->numberBetween(1,5),
                'ticket_price' => $faker->numberBetween(100,200),
                'country' => $faker->country,
                'photo' => ''
            ]);

            $film->comments()->saveMany([
                new \App\Models\Comment([
                    'name' => $faker->name,
                    'user_id' => $i+2,
                    'comment' => $faker->sentence
                ])
            ]);

            $genreName = "Genre" . rand(1,10);
            $genreName2 = "Genre" . rand(11,20);

            $film->genres()->saveMany([
                new \App\Models\Genre([
                    'name' => $genreName,
                    'slug' => str_slug($genreName)
                ]),
                new \App\Models\Genre([
                    'name' => $genreName2,
                    'slug' => str_slug($genreName2)
                ])
            ]);


        }
    }
}
