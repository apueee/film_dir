<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmGenre extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'films_genres';

    /**
     * The attributes that uses for timestamps.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['film_id', 'genre_id'];
}
