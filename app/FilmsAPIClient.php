<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 23/6/18
 * Time: 12:50 PM
 */

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Mockery\Exception;

class FilmsAPIClient
{
    private $client = null;

    private $headers = [
        'Accept' => 'application/json',
        'Content-type' => 'application/json'
    ];


    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('API_URL', 'http://new.films.local/api/'), 'http_errors' => false]);
    }

    public function get($path = '', $params = [])
    {
        try{
            $res = $this->client->request('GET', $path , [
                'headers' => $this->headers,
                'query' => $params
            ]);

            if($res->getStatusCode() == 200) {
                return json_decode($res->getBody(), true);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function post($path = '', $params = [])
    {
        try{
            $response = $this->client->request('POST', $path, [
                'headers' => [
                    'Accept' => 'application/json',
                    'content-type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => $params
            ]);
            if($response->getStatusCode() == 200) {
                return json_decode($response->getBody(), true);
            }
            if($response->getStatusCode() == 422) {
                return json_decode($response->getBody(), true);
            }
            return json_decode(($response->getBody()->getContents()), true);

        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

}