<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilmCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'description' => 'required|max:1200',
            'release_date' => 'required|date_format:Y-m-d H:i:s',
            'rating' => 'required|numeric|between:1,5',
            'ticket_price' => 'required',
            'country' => 'required',
            'genres' => 'required',
            'photo' => 'required'
        ];

        return $rules;
    }
}
