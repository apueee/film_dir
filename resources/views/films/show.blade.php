@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <a href="#">
                    <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset( $film['photo']) }}" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>{{ $film['name'] }}</h3>
                <h5>Rating: <span class="badge badge-success"> {{ $film['rating'] }}/5</span></h5>
                <h5>Release Date: {{ Carbon\Carbon::parse( $film['release_date'])->format('d M, Y')   }}</h5>
                <h5>Country: {{ $film['country']  }}</h5>
                <h5>Ticket Price: ${{ $film['ticket_price'] }}</h5>
                <h6>Genres: @foreach($film['genres'] as $genre) <a><span class="badge badge-info">{{ $genre['name'] }}</span></a>@endforeach</h6>

            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <h2><strong>Description</strong></h2>
                <p>
                    {{ $film['description'] }}
                </p>
            </div>
        </div>
        <h3>Comments</h3>
        @if(Auth::check())
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('films.comments', $film['id']) }}" name="comment-form" method="post">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationServer01">Name</label>
                                    <input type="text" class="form-control" id="validationServer01" placeholder="Name" name="name"  required>

                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="comment">Comment:</label>
                                    <textarea name="comment" class="form-control" rows="3" required id="comment"></textarea>
                                </div>
                            </div>
                            <div class="form-row ">
                                <div class="col-md-12 mb-3">
                                    <br />
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
        @endif
        <hr>
        <div class="row">

            <div class="col-md-12">

                @foreach($film['comments'] as $comment)
                    <div class="row">

                        <div class="col-md-12">
                            <h6>{{ Carbon\Carbon::parse( $comment['created_at'])->diffForHumans() }} - <span class="badge badge-info"> {{ $comment['name'] }}</span></h6>
                            <p>
                                {{ $comment['comment'] }}
                            </p>

                        </div>
                    </div>

                @endforeach
            </div>
        </div>


    </div>

@endsection

@section('pagescript')
    <script type="text/javascript">

        $(document).ready(function() {

            // $('form').on("submit", function (e) {
            //     e.preventDefault();
            //
            //     var name = document.forms["comment-form"]["name"].value;
            //     if (name == "") {
            //         alert("Name must be filled out");
            //
            //     }
            //
            //     alert('hello')
            //
            //     // var districtId = $(this).val();
            //     // var url = "/data/district/" + districtId + "/streets";
            //     //
            //     // $.get(url, function (data) {
            //     //     $('#street').html(data);
            //     // });
            //
            //
            //
            // });

        });

    </script>
@endsection
