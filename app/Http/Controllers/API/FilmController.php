<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\CommentFormRequest;
use App\Http\Requests\FilmCreateRequest;
use App\Models\Comment;
use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilmController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->get('limit', 1);

        return Film::with('genres')->orderBy('id',  'desc')->Paginate($limit);
    }

    public function show( $slug)
    {
        $film = Film::with(['genres', 'comments' => function($query) {
            $query->orderBy('id', 'DESC');
        }])->whereSlug( $slug)->first();

        if(count($film) == 0){
            return response()->json(["message" => "Film not found"], 404);
        }

        return $film;
    }

    public function store(FilmCreateRequest $request)
    {
        $film = new Film();

        $film->name = $request->get('name');
        $film->slug = getUniqueSlug($film, $request->get('name'));
        $film->description = $request->get('description');
        $film->release_date = $request->get('release_date');
        $film->rating = $request->get('rating');
        $film->ticket_price = $request->get('ticket_price');
        $film->country = $request->get('country');

        $file_data = $request->input('photo');
        $filename = 'image_'.time().'.png';

        \Storage::disk('public')->put("images/" . $filename, base64_decode($file_data));

        $film->photo = "storage/images/" .$filename;

        $film->save();

        $genres = explode(',', $request->get('genres'));

        foreach ($genres as $genre) {
            $genre = trim($genre);
            $newGenre = Genre::firstOrCreate([
                'name' => ucfirst($genre),
                'slug' => str_slug($genre)
            ]);

            $film->genres()->attach($newGenre->id);
        }

        return response()->json($film, 201);
    }

    public function StoreComment( CommentFormRequest $request, Film $film)
    {
        $comment = new Comment();

        $comment->name = $request->get('name');
        $comment->comment = $request->get('comment');
        $comment->user_id = $request->get('user_id');

        $film->comments()->save($comment);

        return response()->json($comment, 200);
    }

    public function update(Request $request, Film $film)
    {
//        $film->update($request->all());

        return response()->json($film, 200);
    }

    public function delete(Film $film)
    {
        $film->delete();

        return response()->json(null, 204);
    }
}
