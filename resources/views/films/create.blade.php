@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach ($errors->all() as $error)
            {!! $error !!}
        @endforeach
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('films.store') }}" name="comment-form" enctype='multipart/form-data' method="post">
                            {{ csrf_field() }}

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationServer01">Name</label>
                                    <input type="text" class="form-control {{ $errors->has('name') ? 'is_invalid' : ''}}" value="{{ old('name') }}" placeholder="Name" name="name"  required>
                                    @if ($errors->has('name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('name') }}
                                    </div>
                                    @endif
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="comment">Description</label>
                                    <textarea name="description" class="form-control {{ $errors->has('description') ? 'is_invalid' : ''}}" rows="3" required id="description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('description') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationServer01">Ticket Price</label>
                                    <input type="text" class="form-control {{ $errors->has('ticket_price') ? 'is_invalid' : ''}}" value="{{ old('ticket_price') }}" placeholder="i.e 120" name="ticket_price"  required>
                                    @if ($errors->has('ticket_price'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('ticket_price') }}
                                        </div>
                                    @endif
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationServer01">Release Date</label>
                                    <input type="text" name="release_date" class="form-control datepicker {{ $errors->has('release_date') ? 'is_invalid' : ''}}" value="{{ old('release_date') }}" >
                                    @if ($errors->has('release_date'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('release_date') }}
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationServer01">Genres (Comma separated)</label>
                                    <input type="text" class="form-control {{ $errors->has('genres') ? 'is_invalid' : ''}}" value="{{ old('genres') }}" placeholder="i.e action,comedy" name="genres"  required>
                                    @if ($errors->has('genres'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('genres') }}
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationServer01">Country</label>
                                    <select id="country" name="country" class="form-control">
                                        <option value="" selected="" >Choose Country</option>
                                        @foreach($countries as $country)
                                            <option {{ old('country') == $country->country_name ? 'selected="selected"' : '' }} value="{{ $country->country_name }}">{{ $country->country_name }}</option>
                                        @endforeach
                                    </select>
                                    </label>
                                    @if ($errors->has('country'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('country') }}
                                        </div>
                                    @endif
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label >Rating</label>
                                    <p class="rating ">
                                        <label>
                                            <input type="radio" name="rating" value="1" />
                                            <span class="icon">★</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="2" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="3" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="4" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="5" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                        </label>
                                    </p>
                                    @if ($errors->has('rating'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('rating') }}
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label >Photo</label>

                                    <input type="file" class="form-control-file" name="photo">
                                </div>
                            </div>
                            <div class="form-row ">
                                <div class="col-md-12 mb-3">
                                    <br />
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

@section('pagescript')
    <style type="text/css">
        .rating {
            /*display: inline-block;*/
            position: relative;
            height: 30px;
            line-height: 50px;
            font-size: 30px;
        }

        .rating label {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            cursor: pointer;
        }

        .rating label:last-child {
            position: static;
        }

        .rating label:nth-child(1) {
            z-index: 5;
        }

        .rating label:nth-child(2) {
            z-index: 4;
        }

        .rating label:nth-child(3) {
            z-index: 3;
        }

        .rating label:nth-child(4) {
            z-index: 2;
        }

        .rating label:nth-child(5) {
            z-index: 1;
        }

        .rating label input {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
        }

        .rating label .icon {
            float: left;
            color: transparent;
        }

        .rating label:last-child .icon {
            color: #000;
        }

        .rating:not(:hover) label input:checked ~ .icon,
        .rating:hover label:hover input ~ .icon {
            color: #09f;
        }

        .rating label input:focus:not(:checked) ~ .icon:last-child {
            color: #000;
            text-shadow: 0 0 5px #09f;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function() {
            $('.datepicker').datepicker({
                format:"yyyy-mm-dd 00:00:00"
            });


            $(':radio').change(function() {
                console.log('New star rating: ' + this.value);
            });
            // $('form').on("submit", function (e) {
            //     e.preventDefault();
            //
            //     var name = document.forms["comment-form"]["name"].value;
            //     if (name == "") {
            //         alert("Name must be filled out");
            //
            //     }
            //
            //     alert('hello')
            //
            //     // var districtId = $(this).val();
            //     // var url = "/data/district/" + districtId + "/streets";
            //     //
            //     // $.get(url, function (data) {
            //     //     $('#street').html(data);
            //     // });
            //
            //
            //
            // });

        });

    </script>
@endsection
