<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2/2/18
 * Time: 7:18 PM
 */

namespace App\Http\Controllers;

use App\FilmsAPIClient;
use App\Http\Requests\CommentFormRequest;
use App\Http\Requests\FilmCreateRequest;
use App\Models\Comment;
use App\Models\Country;
use App\Models\Film;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FilmsController extends Controller
{

    public function Index(Request $request)
    {
        $page = $request->get('page', 1);

        $apiClient = new FilmsAPIClient();

        $result =$apiClient->get('films', [
            'page' => $page
        ]);

        return view('films.list', [
            'films' => $result['data'],
            'current_page' => $result['current_page'],
            'last_page' => $result['last_page']
        ]);
    }

    public function create( )
    {
        return view('films.create', [
            'countries' => Country::all()
        ]);
    }

    public function store(FilmCreateRequest $request)
    {

        $apiClient = new FilmsAPIClient();

        $result =$apiClient->post('films',
            [
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'release_date' => $request->get('release_date'),
                'rating' => $request->get('rating'),
                'ticket_price' => $request->get('ticket_price'),
                'country' => $request->get('country'),
                'genres' => $request->get('genres'),
                'photo' => base64_encode(file_get_contents($request->file('photo')->path()))
            ]
        );

        if(isset($result["errors"])) {
            return redirect()->route('films.create')->withInput()->withErrors($result["errors"]);
        }

        return redirect()->route('films.index');
    }
    public function Show($slug)
    {
        $apiClient = new FilmsAPIClient();

        $result =$apiClient->get('films/' . $slug);

        return view('films.show', [
            'film' => $result
        ]);
    }

    public function StoreComment( Request $request, Film $film)
    {
        $apiClient = new FilmsAPIClient();

        $result =$apiClient->post('films/' . $film->id . '/comments',
            [
                'name' => $request->get('name'),
                'comment' => $request->get('comment'),
                'user_id' => Auth::user()->id
            ]
        );

        if(isset($result["errors"])) {
            return redirect()->route('films.show', $film->slug )->withInput()->withErrors($result["errors"]);
        }

        return redirect()->route('films.show', $film->slug);
    }


}