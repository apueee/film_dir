@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($films as $film)
            <div class="row">
                <div class="col-md-7">
                    <a href="#">
                        <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset( $film['photo']) }}" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>{{ $film['name'] }}</h3>
                    <h5>Rating: <span class="badge badge-success"> {{ $film['rating'] }}/5</span></h5>
                    <h5>Release Date: {{ Carbon\Carbon::parse( $film['release_date'])->format('d M, Y')   }}</h5>
                    <h5>Ticket Price: ${{ $film['ticket_price'] }}</h5>
                    <h5>Country: {{ $film['country']  }}</h5>
                    <h6>Genres: @foreach($film['genres'] as $genre) <a><span class="badge badge-info">{{ $genre['name'] }}</span></a>@endforeach</h6>
                    <p>
                    {{ $film['description'] }}
                    </p>
                    <a class="btn btn-primary" href="{{ route('films.show', $film['slug']) }}">Details</a>
                </div>
            </div>

        @endforeach
        <div class="row"><p></p></div>
        <div class="row">
            <div class="col-md-12 ">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item {{ $current_page == 1 ? 'disabled' : '' }}">
                            <a class="page-link" href="?page={{$current_page-1}}" tabindex="-1">Previous</a>
                        </li>
                        @for($i=1; $i <= $last_page; $i++)
                        <li class="page-item {{ $current_page==$i ? 'active' : '' }}"><a class="page-link" href="?page={{$i}}">{{$i}}</a></li>
                        @endfor

                        <li class="page-item {{ $current_page == $last_page ? 'disabled' : '' }}">
                            <a class="page-link" href="?page={{$current_page+1}}">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection